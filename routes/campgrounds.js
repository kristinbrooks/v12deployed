// *** CAMPGROUND ROUTES ***

let express = require('express'),
  router = express.Router(),
  Campground = require('../models/campground'),
  middleware = require('../middleware');

// INDEX
router.get('/', (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log(err);
      req.flash('error', 'Something went wrong');
    } else {
      res.render('campgrounds/index', {campgrounds: allCampgrounds});
    }
  });
});

// NEW
router.get('/new', middleware.isLoggedIn, (req, res) => {
  res.render('campgrounds/new');
});

// CREATE
router.post('/', middleware.isLoggedIn, (req, res) => {
  let name = req.body.name;
  let price = req.body.price;
  let image = req.body.image;
  let desc = req.body.description;
  let author = {
    id: req.user._id,
    username: req.user.username,
  };
  let newCampground = {name: name, price: price, image: image, description: desc, author: author};

  Campground.create(newCampground, (err) => {
    if (err) {
      console.log(err);
      req.flash('error', 'Something went wrong');
    } else {
      req.flash('success', 'Successfully added a new campground!');
      res.redirect('/campgrounds');
    }
  });
});

// SHOW
router.get('/:id', (req, res) => {
  Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
    if (err) {
      console.log(err);
      req.flash('error', 'Something went wrong');
    } else {
      res.render('campgrounds/show', {campground: foundCampground});
    }
  });
});

// EDIT
router.get('/:id/edit', middleware.checkCampgroundOwnership, (req, res) => {
  // only gets here if checkCampgroundOwnership was true and went to next()
  Campground.findById(req.params.id, (err, foundCampground) => {
    res.render('campgrounds/edit', {campground: foundCampground});
  });
});

// UPDATE
router.put('/:id', middleware.checkCampgroundOwnership, (req, res) => {
  // find and update the correct campground
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground) => {
    if (err) {
      console.log(err);
      req.flash('error', 'Something went wrong');
      res.redirect('/campgrounds');
    } else {
      res.redirect('/campgrounds/' + updatedCampground._id);
    }
  });
});

// DESTROY
router.delete('/:id', middleware.checkCampgroundOwnership, async (req, res) => {
  try {
    let foundCampground = await Campground.findById(req.params.id);
    await foundCampground.remove();
    req.flash('error', 'Campground deleted');
    res.redirect('/campgrounds');
  } catch {
    console.log(error.message);
    req.flash('error', 'Something went wrong');
    res.redirect('/campgrounds');
  }
});

module.exports = router;
