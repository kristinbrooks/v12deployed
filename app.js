// ***** YelpCamp App *****

// -------------------
//  GENERAL REQUIRES
// -------------------

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  flash = require('connect-flash'),
  methodOverride = require('method-override'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  LocalStrategy = require('passport-local'),
  seedDB = require('./seeds'),
  User = require('./models/user');


// -------------------
//   ROUTE REQUIRES
// -------------------

let campgroundRoutes = require('./routes/campgrounds'),
  commentsRoutes = require('./routes/comments'),
  indexRoutes = require('./routes/index');


// -------------------
//   GENERAL CONFIG
// -------------------

// `process.env.VARIABLE` is how you access an environment variable in Node.js
  // good to use because they keep data hidden/obscured --- so if others have access to the code
  // they would have to set up their own environment variable

// we set DATABASEURL locally in the terminal with:
// `export DATABASEURL=mongodb://localhost:27017/yelp_camp`

// in Heroku, we went to Settings->Config Vars and added:
// Key = DATABASEURL  and
// Value = mongodb+srv://Kristin:CARS%21muck6phoy4klop@sandbox.fmvza.mongodb.net/yelp_camp?retryWrites=true&w=majority

// can also set via command line using these instructions: https://devcenter.heroku.com/articles/nodejs-support#customizing-the-build-process

// this gives us a backup with the `||`
let url = process.env.DATABASEURL || 'mongodb://localhost:27017/yelp_camp';
mongoose.connect(url, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.log(error.message));

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));
app.use(flash());
app.use(methodOverride('_method'));


// -------------------
//    SEED DATABASE
// -------------------

// seedDB();


// -------------------
//   PASSPORT CONFIG
// -------------------

app.use(require('express-session')({
  secret: 'I miss my sweet Tali Tigershark <3',
  resave: false,
  saveUninitialized: false,
}));
app.use(passport.initialize({}));
app.use(passport.session({}));
passport.use(new LocalStrategy({}, User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser((User.deserializeUser()));
app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  res.locals.error = req.flash('error');  // passes it to all templates
  res.locals.success = req.flash('success');  // passes it to all templates
  next();
});


// -------------------
//    ROUTES CONFIG
// -------------------

app.use(indexRoutes);
app.use('/campgrounds', campgroundRoutes);
app.use('/campgrounds/:id/comments', commentsRoutes);


// -------------------
//      SERVER
// -------------------

let port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('The YelpCamp server has started...');
});
